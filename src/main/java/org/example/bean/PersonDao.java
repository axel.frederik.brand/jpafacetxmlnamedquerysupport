package org.example.bean;

import org.example.model.Person;
import org.example.qualifier.DataRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Simple PersonDao bean that handles persisting a person entity and returning a
 * list of people in the database. Implemented as an EJB for the transaction
 * facilities.
 */
@Stateless
public class PersonDao {

    @Inject
    @DataRepository
    private EntityManager entityManager;

    public void savePerson(Person p) {
        entityManager.persist(p);
    }

    @SuppressWarnings("unchecked")
    public List<Person> getPeople() {
        return entityManager.createQuery("select p from Person p")
                .getResultList();
    }


    /**
     * For ALl Queries Code completion is working, refactoring when renaming Entity properties is working.
     *
     * QueryParam Name checking and Refactoring not working when using NamedQueries
     */

    public Person getPersonForIdNamedEntity(Long id) {
        //will show error when nam9e is not correct
        TypedQuery<Person> query = entityManager.createNamedQuery("testNamedEntity", Person.class);
        TypedQuery<Person> query2 = entityManager.createNamedQuery("testNamedEntityyyy", Person.class);

        //will not refactor when Action -> Renaming :id in query
        query.setParameter("id", id);

        //does produce 'Cannot resolve query parameter 'idd'
         query.setParameter("idd", id);
        return query.getSingleResult();
    }

    public Person getPersonForIdNamedXML(Long id) {
        // will show error when name is not correct
        TypedQuery<Person> query = entityManager.createNamedQuery("testNamedXML", Person.class);
         TypedQuery<Person> query2 = entityManager.createNamedQuery("testNamedXMLLLL", Person.class);
        //will not refactor when Action -> Renaming :id in query
        query.setParameter("id", id);

        //does not produce 'Cannot resolve query parameter 'idd'
         query.setParameter("idd", id);
        return query.getSingleResult();
    }

    public Person getPersonForIdDynamic(Long id) {
        TypedQuery<Person> query = entityManager.createQuery("select p from Person p where p.id = :id", Person.class);
        // will refactor when Action -> Renaming :id in query
        query.setParameter("id", id);

        // produces "Cannot resolve query parameter 'idd'"
         query.setParameter("idd", id);
        return query.getSingleResult();
    }
}
